# Imports:
import pygame
import time
import random
# Setup:
pygame.init() #Initializes pygame module

#Anything That is Text Related:
pygame.display.set_caption("RETROLAND") #This sets the heading of the window, I will use this to display the name of the game.
font = pygame.font.Font("Images/DOTMATRI.TTF", 50) #Sets the font of any text to whatever the default system font is. pygame.font.SysFont(Name of font, size, bold,italics)

#The code underneath is related to anything that has to do to the game's size.
display_width=800 #Sets the Width of the Game Window
display_height=600#Sets the height of the game window.
block_size = 10 #Sets the value for the size of a single block in the SNEK Game.
gameDisplay =  pygame.display.set_mode((display_width,display_height)) #This sets the game window size
pygame.display.update() # Updates the Display to the newest frame. pygame.display.flip also does the same thing as this, but has less versatility.

#The code undeneath monitors the Frames Per second and the overall "speed of the game".
clock = pygame.time.Clock() #Makes a clock that monitors the Frames per Second.
FPS = 20 #Sets the value of the Frames Per Second.

#The code below initializes and plays audio!!
pygame.mixer.init() #Initializes the Mixer/Player.
pygame.mixer.music.load("Sounds/Blip Stream.mp3") #Loads the audio file
pygame.mixer.music.play(-1) #Plays the audio file and loops it forever.

# The code underneath sets variables for colours:
white = (255,255,255)
black = (0,0,0)
red = (255,0,0)
brightGreen = (3, 254, 1)
snakeGreen = (120,154,19)
poisonblue = (25, 25, 77)

#Internal Variables that will change whether the game runs:
gameQuit = False #Sets the variable that will check if the user wants to exit the game to false.
gameExit = False #This will check if the user wants to stop the game, but not quit it. It is set to true.

def StartScreen(): #Defines the Code for the Start Screen
    global gameQuit #Selects the global value of Gamequit
    global font
    StartImage = pygame.image.load('Images/welcomescreen.png') #Sets "StartImage" To the specified image
    while gameQuit==False: #When the game is running:
        gameDisplay.fill(black) #Turn the Screen White
        gameDisplay.blit(StartImage, (0,0)) #Put the Pre'specified image onto the screen.
        pygame.display.update() #Update the screen to show the newest frame
        for event in pygame.event.get(): #If a key is pressed or the mouse is moved.
            if event.type == pygame.QUIT: #If the event is the quit button being clicked
               pygame.quit()
               quit() #Quit the game
            elif event.type == pygame.KEYDOWN: #If the event is a key being pushed down:
                if event.key == pygame.K_m: #if the key being pushed down is the letter "p"
                    MenuScreen() #call The MenuScreen()
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    quit()

def MenuScreen():
    global gameQuit #Selects the global value of Gamequit
    global font
    global gameExit
    MenuImage = pygame.image.load('Images/MenuScreen.png') #Sets "StartImage" To the specified image
    while gameQuit==False: #When the game is running:
        gameDisplay.fill(black) #Turn the Screen White
        gameDisplay.blit(MenuImage, (0,0)) #Put the Pre'specified image onto the screen.
        pygame.display.update() #Update the screen to show the newest frame
        for event in pygame.event.get(): #If a key is pressed or the mouse is moved.
            if event.type == pygame.QUIT: #If the event is the quit button being clicked
               pygame.quit()
               quit() #Quit the game
            elif event.type == pygame.KEYDOWN: #If the event is a key being pushed down:
                if event.key == pygame.K_s: #if the key being pushed down is the letter "p"
                    gameExit = False
                    gameQuit = False
                    ClassicSnake() #call The MenuScreen()
                if event.key == pygame.K_r:
                    gameExit = False
                    gameQuit = False
                    RetardedSnake()
                if event.key == pygame.K_ESCAPE:
                    StartScreen()

def GameOverScreenR(score): #Defines the Code for the Gameover Screen for the Retarded Snake
    global gameQuit #Selects the global value of Gamequit
    global gameExit
    pygame.mixer.music.set_volume(0.5) #Sets the Volume to Half
    GameOverImage = pygame.image.load('Images/GameOverScreen.png') #Sets "StartImage" To the specified image
    scoretext=font.render("Score: "+str(score), 1,brightGreen) #Renders the Score Onto the screen.
    while True:
        gameDisplay.fill(black) #Turn the Screen White
        gameDisplay.blit(GameOverImage, (0,0)) #Put the Pre'specified image onto the screen.
        gameDisplay.blit(scoretext, (330, 360))
        pygame.display.update() #Update the screen to show the newest frame
        
        for event in pygame.event.get(): #If a key is pressed or the mouse is moved.
            if event.type == pygame.QUIT: #If the event is the quit button being clicked
                pygame.quit() #for some reason, I was unable to set gameQuit to true and quit the game that way, so I'm doing it like this.
                quit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    gameExit = False
                    RetardedSnake()
                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                if event.key == pygame.K_m:
                    MenuScreen()

def GameOverScreenC(score): #Defines the code for the gameover screen for the Classic Snake
    global gameQuit #Selects the global value of Gamequit
    global gameExit
    pygame.mixer.music.set_volume(0.5) #Sets the Volume to Half
    GameOverImage = pygame.image.load('Images/GameOverScreen.png') #Sets "StartImage" To the specified image
    scoretext=font.render("Score: "+str(score), 1,brightGreen) #Renders the Score Onto the screen.
    while True:
        gameDisplay.fill(black) #Turn the Screen White
        gameDisplay.blit(GameOverImage, (0,0)) #Put the Pre'specified image onto the screen.
        gameDisplay.blit(scoretext, (330, 360))
        pygame.display.update() #Update the screen to show the newest frame
        
        for event in pygame.event.get(): #If a key is pressed or the mouse is moved.
            if event.type == pygame.QUIT: #If the event is the quit button being clicked
                pygame.quit() #for some reason, I was unable to set gameQuit to true and quit the game that way, so I'm doing it like this.
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    gameExit = False
                    ClassicSnake()
                if event.key == pygame.K_q:
                    pygame.quit()
                    quit()
                if event.key == pygame.K_m:
                    MenuScreen()

def ClassicSnake(): #This is the code for the classc version of snake.
    #Game Code for the classical snake game  Starts Here:
    global gameQuit #This calls the the gameQuit variable, and recognizes it as the global one that was set previously.
    global gameExit #This calls the the gameExit variable, and recognizes it as the global one that was set previously.
    global block_size #Calls the blocksize variable and recognizes it as the global one that was set previously.
    welcomescreen = pygame.image.load("Images/ClassicSnakeStartScreen.png")
    #This defines herosnake, which draws our character(the snake onto the screen)
    def HeroSnake(block_size, snakelist):
        for XnY in snakelist:
            snake = pygame.draw.rect(gameDisplay, snakeGreen, [XnY[0],XnY[1],block_size,block_size]) #Draws a rectangle on the screen pygame.draw.rect(which canvas, what colour, [x position,y position,width,height])

    #Any Variables That Will Change within the snake Game itself are Put Here:
    
    #The code underneath sets variables for the 'snake' character.
    lead_x = display_width/2 #Sets the X Position of the head of the snake
    lead_y = display_height/2 #Sets the y position of the head of the snake.
    lead_x_change = 0 #Sets the change that occurs to the X Position of the head of the snake to Zero.
    lead_y_change = 0 #Sets the change that occurs to the Y Position of the head of the snake to Zero.
    snakeList = [] #Makes a list (which I will use to hold the locations of the snake)
    snakeLength = 1 #This is the length of the snake at any given moment(This may change)
    
    #The Following Code is anything that has to do with the apple(s) that the snake eats
    randApple_x = (round(random.randrange(block_size,display_width-block_size)/10.0)*10.0) #Sets the X Pos of the apple to somewhere between the left and right and aligns it to the snakes head, more info at the bottom of code.
    randApple_y = (round(random.randrange(block_size,display_height-block_size)/10.0)*10.0)# Sets the Y pos of the apple to somewhere between the top and bottom and aligns it to the snakes head, more info at the bottom of code.
    randPoisonApple_x = (round(random.randrange(block_size,display_width-block_size)/10.0)*10.0) #Sets the X Pos of the apple to somewhere between the left and right and aligns it to the snakes head, more info at the bottom of code.
    randPoisonApple_y = (round(random.randrange(block_size,display_height-block_size)/10.0)*10.0)# Sets the Y pos of the apple to somewhere between the top and bottom and aligns it to the snakes head, more info at the bottom of code.
    appleThickness = 10
    apples_eaten = 0 #Sets the Number of Apples that are eaten by the snake to Zero.
    pygame.mixer.music.set_volume(1.0) #Sets the Volume to Full
    #Game Logic Starts Here
    while True: #While True
        gameDisplay.blit(welcomescreen,(0,0)) #Blit the welcomescreen onto the gamedisplay
        pygame.display.update() #Update the display
        for event in pygame.event.get(): #If a key is pressed or the mouse is moved.
            if event.type == pygame.QUIT: #If the event is the quit button being clicked
               pygame.quit()#Quit Pygame
               quit()
            if event.type == pygame.KEYDOWN: #If a key is pressed
                if event.key == pygame.K_p: #if the key is "P"
                    while True: #Start Main Logic Loop

                        clock.tick(FPS) #Sets the Frames per Second to the value of FPS

                        gameDisplay.fill(black)#Turns the screen white

                        apple = pygame.draw.rect(gameDisplay, red, [randApple_x ,randApple_y,appleThickness,appleThickness]) #Draws an Apple
                        poisonapple = pygame.draw.rect(gameDisplay, poisonblue, [randPoisonApple_x ,randPoisonApple_y,appleThickness,appleThickness]) # draws a poison apple

                        snakeHead = [] #makes a list which I will use to store the locations of the snake's head
                        snakeHead.append(lead_x) #Adds lead_x to snake head
                        snakeHead.append(lead_y) #Adds lead_y to snake hed
                        snakeList.append(snakeHead) #adds snake head to the list of locations
                        if len(snakeList) > snakeLength: #If the length of snakelist is bigger than the allowed snakelength.
                            del snakeList[0] #Delete the first entry of  snakelength


                        HeroSnake(block_size,snakeList) #Calls Herosnake, which draws the snake and sets the leadx and y coordinates to the ones set in thsi function.
                        pygame.display.update() # Updates the Screen


                        for event in pygame.event.get(): #This checks the keypress/mouse movement and gives the correct response.
                            #print (event) #Uncomment this if you want to see all the events that occur within pygame.
                            if event.type == pygame.QUIT: #If the user presses the exit butten in the top right/left.
                                gameQuit = True           #Make gameQuit= true
                            if event.type == pygame.KEYDOWN: #Makes the Snake Move If a key is pushed down.
                                if event.key == pygame.K_LEFT or event.key == pygame.K_a: # if the LEFT Key or the A key is pushed down and not let up:
                                    lead_x_change = -block_size #Set the change to the x position to -(whatever the block size is)
                                    lead_y_change = 0 #Setting lead_y_change to zero will ensure that no diagonal movement will take place. Comment this if you want snake to be MUCH harder.Further Explanation is at the bottom of the code.
                                elif event.key == pygame.K_RIGHT or event.key == pygame.K_d: # if the RIGHT Key is pushed down and not let up:
                                    lead_x_change =  block_size #Set the change to the x position to +(Whatever the block size is)
                                    lead_y_change = 0 #Setting lead_y_change to zero will ensure that no diagonal movement will take place. Comment this if you want snake to be MUCH harder.Further Explanation is at the bottom of the code.
                                elif event.key == pygame.K_UP or event.key == pygame.K_w:   # if the UP Key is pushed down and not let up
                                    lead_y_change = -block_size # Subtract (whatever the blocksize is) from the y position
                                    lead_x_change = 0 #Setting lead_x_change to zero will ensure that no diagonal movement will take place. Comment this if you want snake to be MUCH harder.Further Explanation is at the bottom of the code.
                                elif event.key == pygame.K_DOWN or event.key == pygame.K_s: # if the DOWN Key is pushed down and not let up
                                    lead_y_change = block_size  # Add (Whatever the blocksize is) to the y position
                                    lead_x_change = 0 #Setting lead_x_change to zero will ensure that no diagonal movement will take place. Comment this if you want snake to be MUCH harder.Further Explanation is at the bottom of the code.
                        lead_x += lead_x_change #Adds the change to lead_x to lead_x, which then changes the location of the snake head on screen.
                        lead_y += lead_y_change #Adds the change to lead_y to lead_y, which then changes the location of the snake head on screen.
                                
                                #Uncomment ALL THE LINES of the following code (except the explaining text), if you want the snake to stop whenever you let the arrow/WASD Keys go after pressing them.
                            #if event.type == pygame.KEYUP:  #Makes the Snake stop If the key that was held down is released. 
                                #if event.key == pygame.K_LEFT or pygame.K_RIGHT or event.key == pygame.K_a or event.key == pygame.K_d:
                                    #lead_x_change = 0
                                #if event.key == pygame.K_UP or pygame.K_DOWN or event.key == pygame.K_s or event.key == pygame.K_w:
                                    #lead_y_change = 0
                        
                        if lead_x == randApple_x and lead_y == randApple_y:
                            apples_eaten += 1 #Add one to the number of apples eaten
                            snakeLength += 1 #add 1 to the alllowed snakelength
                            # print(apples_eaten) #Uncomment this if you want to see the umber of apples eaten per round.
                            randApple_x = (round(random.randrange(0,display_width-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
                            randApple_y = (round(random.randrange(0,display_height-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
                            randPoisonApple_x = (round(random.randrange(0,display_width-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
                            randPoisonApple_y = (round(random.randrange(0,display_height-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
                        if lead_x >= display_width or lead_y >= display_height or lead_x <= 0 or lead_y <= 0 : #This code quits the game if the player has hit the boundary of the game windows.
                            gameExit = True #Sets gameExit to true (I mean, you did exit the game after all!)
                            GameOverScreenC(apples_eaten) #Calls The GameOverScreen
                        if snakeHead in snakeList[:-1]:
                            GameOverScreenC(apples_eaten)
                        if lead_x == randPoisonApple_x and lead_y == randPoisonApple_y:
                            GameOverScreenC(apples_eaten)
                    #End Of Game Code          

def RetardedSnake(): #This is the code for my RETARDED Version of snake.
        #Game Code Starts Here:
    global gameQuit #This calls the the gameQuit variable, and recognizes it as the global one that was set previously.
    global gameExit #This calls the the gameExit variable, and recognizes it as the global one that was set previously.
    global block_size #Calls the blocksize variable and recognizes it as the global one that was set previously.
    welcomescreen = pygame.image.load("Images/RetardedSnakeStartScreen.png")
    #The code underneath sets variables for the 'snake' character.
    lead_x = display_width/2 #Sets the X Position of the head of the snake
    lead_y = display_height/2 #Sets the y position of the head of the snake.
    lead_x_change = 0 #Sets the change that occurs to the X Position of the head of the snake to Zero.
    lead_y_change = 0 #Sets the change that occurs to the Y Position of the head of the snake to Zero.
    snakeList = [] #Makes a list (which I will use to hold the locations of the snake)
    snakeLength = 1 #This is the length of the snake at any given moment(This may change)

    #The Following Code is anything that has to do with the apple(s) that the snake eats
    appleThickness = 20
    randApple_x = (round(random.randrange(block_size,display_width-appleThickness)/10.0)*10.0) #Sets the X Pos of the apple to somewhere between the left and right and aligns it to the snakes head, more info at the bottom of code.
    randApple_y = (round(random.randrange(block_size,display_height-appleThickness)/10.0)*10.0)# Sets the Y pos of the apple to somewhere between the top and bottom and aligns it to the snakes head, more info at the bottom of code.
    randPoisonApple_x = (round(random.randrange(0,display_width-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
    randPoisonApple_y = (round(random.randrange(0,display_height-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
    appleThickness = 20
    apples_eaten = 0 #Sets the Number of Apples that are eaten by the snake to Zero.
    pygame.mixer.music.set_volume(1.0) #Sets the Volume to Full
    
    #Game Logic Starts Here
    while True: #While True
        gameDisplay.blit(welcomescreen,(0,0)) #Blit the welcomescreen onto the gamedisplay
        pygame.display.update() #Update the display
        for event in pygame.event.get(): #If a key is pressed or the mouse is moved.
            if event.type == pygame.QUIT: #If the event is the quit button being clicked
               pygame.quit()#Quit Pygame
               quit()      #Quit Python
            if event.type == pygame.KEYDOWN: #If a key is pressed
                if event.key == pygame.K_p: #if the key is "P"
                    while True: #When The Game is running, execute the following code

                        clock.tick(FPS) #Sets the Frames per Second to the value of FPS

                        gameDisplay.fill(black)#Turns the screen black

                        apple = pygame.draw.rect(gameDisplay, red, [randApple_x ,randApple_y,appleThickness,appleThickness]) #Draws an Apple
                        poisonapple = pygame.draw.rect(gameDisplay, poisonblue, [randPoisonApple_x ,randPoisonApple_y,appleThickness,appleThickness]) # draws a poison apple

                        #The Code Underneath is all code for and about the snake Info about this is at the bottom of the code.
                        snakeHead = [] #makes a list which I will use to store the locations of the snake's head
                        snakeHead.append(lead_x) #Adds lead_x to snake head
                        snakeHead.append(lead_y) #Adds lead_y to snake hed
                        snakeList.append(snakeHead) #adds snake head to the list of locations
                        if len(snakeList) > snakeLength: #If the length of snakelist is bigger than the allowed snakelength.Delete the first entry of  snakelength
                            del snakeList[0]
                        for XnY in snakeList: #For every entry in SnakeList
                            snake = pygame.draw.rect(gameDisplay, snakeGreen, [XnY[0],XnY[1],block_size,block_size]) #Draw a segment of the snake.

                        pygame.display.update() # Updates the Screen


                        for event in pygame.event.get(): #This checks the keypress/mouse movement and gives the correct response.
                            #print (event) #Uncomment this if you want to see all the events that occur within pygame.
                            if event.type == pygame.QUIT: #If the user presses the exit butten in the top right/left.
                                gameQuit = True           #Make gameQuit= true
                            if event.type == pygame.KEYDOWN: #Makes the Snake Move If a key is pushed down.
                                if event.key == pygame.K_LEFT or event.key == pygame.K_a: # if the LEFT Key or the A key is pushed down and not let up:
                                    lead_x_change = -block_size #Set the change to the x position to -(whatever the block size is)
                                    #lead_y_change = 0 #Setting lead_x_change to zero will ensure that no diagonal movement will take place. Diagonal movement is prevented in classicsnek. Further Explanation is at the bottom of the code.
                                elif event.key == pygame.K_RIGHT or event.key == pygame.K_d: # if the RIGHT Key is pushed down and not let up:
                                    lead_x_change =  block_size #Set the change to the x position to +(Whatever the block size is)
                                    #lead_y_change = 0 #Setting lead_x_change to zero will ensure that no diagonal movement will take place. Diagonal movement is prevented in classicsnek. Further Explanation is at the bottom of the code.
                                elif event.key == pygame.K_UP or event.key == pygame.K_w:   # if the UP Key is pushed down and not let up
                                    lead_y_change = -block_size # Subtract (whatever the blocksize is) from the y position
                                    #lead_x_change = 0 #Setting lead_x_change to zero will ensure that no diagonal movement will take place. Diagonal movement is prevented in classicsnek. Further Explanation is at the bottom of the code.
                                elif event.key == pygame.K_DOWN or event.key == pygame.K_s: # if the DOWN Key is pushed down and not let up
                                    lead_y_change = block_size  # Add (Whatever the blocksize is) to the y position
                                    #lead_x_change = 0 #Setting lead_x_change to zero will ensure that no diagonal movement will take place. Diagonal movement is prevented in classicsnek. Further Explanation is at the bottom of the code.
                        lead_x += lead_x_change #Adds the change to lead_x to lead_x, which then changes the location of the snake head on screen.
                        lead_y += lead_y_change #Adds the change to lead_y to lead_y, which then changes the location of the snake head on screen.
                                
                                #Uncomment ALL THE LINES of the following code (except the explaining text), if you want the snake to stop whenever you let the arrow/WASD Keys go after pressing them.
                            #if event.type == pygame.KEYUP:  #Makes the Snake stop If the key that was held down is released. 
                                #if event.key == pygame.K_LEFT or pygame.K_RIGHT or event.key == pygame.K_a or event.key == pygame.K_d:
                                    #lead_x_change = 0
                                #if event.key == pygame.K_UP or pygame.K_DOWN or event.key == pygame.K_s or event.key == pygame.K_w:
                                    #lead_y_change = 0
                        
                        if snake.colliderect(apple): #if the snake collides with the apple.
                            apples_eaten += 1 #Add one to the number of apples eaten
                            snakeLength += 1 #add 1 to the alllowed snakelength
                            # print(apples_eaten) #Uncomment this if you want to see the umber of apples eaten per round.
                            randApple_x = (round(random.randrange(0,display_width-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
                            randApple_y = (round(random.randrange(0,display_height-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
                            randPoisonApple_x = (round(random.randrange(0,display_width-block_size)/10.0)*10.0) #Makes a new apple with new coordinates
                            randPoisonApple_y = (round(random.randrange(0,display_height-block_size)/10.0)*10.0) #Makes a new apple with new coordinates

                        if lead_x >= display_width or lead_y >= display_height or lead_x <= 0 or lead_y <= 0 : #This code quits the game if the player has hit the boundary of the game windows.
                            gameExit = True #Sets gameExit to true (I mean, you did exit the game after all!)
                            GameOverScreenR(apples_eaten) #Calls The GameOverScreen
                        if snakeHead in snakeList[:-1]: #f you cross over with yourself
                            gameExit = True
                            GameOverScreenR(apples_eaten) #GameOver
                        if snake.colliderect(poisonapple):
                            GameOverScreenR(apples_eaten)
    #End Of Game Code          

StartScreen() #Calls StartScreen

if gameQuit == True:
    pygame.quit()
    quit()
# On the subject of the diagonal movement of the snake:
    # When you comment the code that prevents this from happening, 
    # what happens is that movement that goes horizontally is shown alongside vertical movement,
    # giving you the illusion of diagonal movement.
    # The prevention-code sets Horizontal movement to zero, if vertical
    # movement has occured, and vice versa if horizontal movement has occured,
    # eliminating all possibility of them happening at the same time.
    # Thus removing any chance of diagonal movement.
    # While snake with diagonal movement is very fun to play, it is NOT
    # traditional snake, so it has been left out. Feel free to modify the code as necessary.
# On the subject of the positioning of the apple:z
     #When you don't round the x/y values, the apple is ever so slightly out of sync with the coordinates
     #of the snake, making it impossible to detect any collison (I am currently working on a way to avoid this)
     #If you round the coordinates, it will snap the location of the apple to an invisible "grid" of sorts
     # making sure the snake completely covers it if it tries to eat the apple.
     #The Reason I've made the range between 10 and 790 for the width and between 10 and 590 for the length
     #Is to make sure that the apple doesnt appear at the very border of the game, making it impossible(well, EXTREMELY FRUSTRATING)
#On the subject of Retarded Snake:
    # I've had to make some major differences about how the game works.
    # Arguably, this version (the way I implement it, not the actual game itself)
    # is Better, as it doesnt need the apple to be in sync with the snake.
    # There are a lot of bugs in there, and I'm trying to weed them out.