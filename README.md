# RetroLand

RetroLand is an attempt to port multiple Retro games such as Snake™ and Pacman™ to Python 3 with the help of pygame as a learning exercise.


### Prerequisites

Retroland runs on Python 3.x

Retroland uses the pygame extention to render graphics and audio among other things. Pygame is the only 3rd Party dependency.
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install pygame.

```bash
pip install pygame
```
or, if you have a separate Python3 installation

```bash
pip3 install pygame
```

### Installing
Installation instructions vary from OS to OS but the general gist is as follows:

Get prerequisites.

Clone RetroLand Repository.

Execute Python file [Retroland.py] from within the directory.

## Authors

* **Aathish Sivasubrahmanian** - *Initial work, copying from tutorial ;)*

See also the list of [contributors](https://gitlab.com/aathish04/RetroLand/-/graphs/master) who participated in this project.


## Acknowledgments

* Most of the code for the Snake and Retarded Snake games was written by following thenewboston's youtube series with sentdex on using pygame